keepalived:
  config: |
{% raw %}
          global_defs {
            notification_email {
              test@test.com
            }
            notification_email_from keepalived-centos-74.vagrantup.com@test.com
            router_id CHK_TEST
            smtp_connect_timeout 30
            smtp_server 127.0.0.1
          }
          vrrp_instance VI_1 {
            advert_int 1
            authentication {
              auth_pass 8712
              auth_type PASS
            }
            interface eth0
            priority 100
            state MASTER
            virtual_ipaddress {
              127.0.1.2
            }
            virtual_router_id 61
          }
          vrrp_script chk_consul {
            interval 2
            script /srv/scripts/keepalived/check_consul.sh
            weight 2
          }
{% endraw %}

keepalived_check:
  chk:
    check_consul:
      secret: test
      script: |
{% raw %}
          #!/bin/bash
          STATUS=$(curl -s --noproxy 127.0.0.1 127.0.0.1:8500/v1/health/node/{{ grains.host }}?token={{ keepalived_check.check_secret }} | grep -oh "alive"| cut -d " " -f

          if [ "$STATUS" == "alive" ]
          then
            exit 0
          else
            exit 1
          fi
{% endraw %}
