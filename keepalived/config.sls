{% from "keepalived/map.jinja" import keepalived with context %}

keepalived-config:
  file.managed:
    - name: /etc/keepalived/keepalived.conf
    - user: root
    - group: root
    - mode: 0644
    - contents: |
      {{ keepalived.config | indent(8) }}
