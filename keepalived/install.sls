keepalived.install:
  pkg.installed:
    - name: keepalived

{# looks like the dependency libipset3 is missing on Debian machines so we install it here#}
{% if grains['os_family'] == 'Debian' %}
libipset3:
  pkg.installed:
    - name: libipset3
    - require_in:
      - pkg: keepalived
{% endif %}
